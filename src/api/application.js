import request from '@/utils/request'

const applicationApi = {
    Create: '/v1/app/create',
    Delete: '/v1/app/delete',
    GetAppById: '/v1/app/get',
    Update: '/v1/app/update',
    GetAppList: '/v1/app/page'
}

export function createApp (parameter) {
  return request({
    url: applicationApi.Create,
    method: 'post',
    data: parameter
  })
}

export function deleteApp (parameter) {
  return request({
    url: applicationApi.Delete,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}

export function updateApp (parameter) {
  return request({
    url: applicationApi.Update,
    data: parameter,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function getAppById (parameter) {
  return request({
    url: applicationApi.GetAppById,
    data: parameter,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function getAppList (parameter) {
  return request({
    url: applicationApi.GetAppList,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}
