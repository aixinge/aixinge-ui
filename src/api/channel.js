import request from '@/utils/request'

const channelApi = {
    Create: '/v1/channel/create',
    Delete: '/v1/channel/delete',
    GetChannelById: '/v1/channel/get',
    Update: '/v1/channel/update',
    GetChannelList: '/v1/channel/page'
}

export function createChannel (parameter) {
  return request({
    url: channelApi.Create,
    method: 'post',
    data: parameter
  })
}

export function deleteChannel (parameter) {
  return request({
    url: channelApi.Delete,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}

export function updateChannel (parameter) {
  return request({
    url: channelApi.Update,
    data: parameter,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function getChannelById (parameter) {
  return request({
    url: channelApi.GetChannelById,
    data: parameter,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function getChannelList (parameter) {
  return request({
    url: channelApi.GetChannelList,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}
